﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.DataVisualization.Charting;
using System.Threading;

using System.IO;
using Microsoft.Win32;

using Alghoritm;

namespace Inzynierka_Implementacja
{
    /// <summary>
    /// Interface for alghoritm class
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<KeyValuePair<int, double>> SItems { get; set; }
        public ObservableCollection<KeyValuePair<int, double>> IItems { get; set; }
        public ObservableCollection<KeyValuePair<int, double>> RItems { get; set; }

        public ObservableCollection<KeyValuePair<long, long>> ResultItems { get; set; }
        public ObservableCollection<KeyValuePair<long, long>> AverageItems { get; set; }
        public ObservableCollection<KeyValuePair<long, long>> WorstItems { get; set; }

        //Testing model data (no optimalization)
        private bool everTestRuned;
        private bool modelTested;
        private EpidemicsModel em = new EpidemicsModel();
        double beta;
        double kappa;
        long IBeg;
        long epidemicsPopulation;
        int epidemicsLength;

        //alghoritm parameters
        Alghoritm.Alghoritm.Selection selectionMethod;
        long populationCount;
        double MutationProb;
        double simpleCrossProb;
        int seed;

        int vacCost;
        int hospCost;
        int dedCost;
        long maxIt;
        long maxDiff;

        int maxVac;
        int maxHosp;
        double vacEffct;
        double hospEffect;
        double lambda;

        //algorithm class
        Alghoritm.Alghoritm al = new Alghoritm.Alghoritm();

        /// <summary>
        /// window constructor
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            SItems = new ObservableCollection<KeyValuePair<int, double>>();
            IItems = new ObservableCollection<KeyValuePair<int, double>>();
            RItems = new ObservableCollection<KeyValuePair<int, double>>();
            ResultItems = new ObservableCollection<KeyValuePair<long, long>>();
            WorstItems = new ObservableCollection<KeyValuePair<long, long>>();
            AverageItems = new ObservableCollection<KeyValuePair<long, long>>();
            modelTested = false;
            everTestRuned = false;
            this.DataContext = this;
            AddToLog("Inicjalizacja programu");
            
        }

        /// <summary>
        /// validation functions, sets background color of textbox
        /// -to red when data is invalid
        /// -to normal when data is valid 
        /// </summary>
        /// <param name="tb">
        /// TextBox class
        /// </param>
        /// <param name="isGood">
        /// true - valida data, false - invalida data
        /// </param>
        private void SetTBState(TextBox tb, bool isGood)
        {
            if(isGood)
                tb.Background = new SolidColorBrush(Color.FromArgb(255,255,255,255));
            else
                tb.Background = new SolidColorBrush(Color.FromArgb(255,255,128,128));
        }

        /// <summary>
        /// validation function construct information string for invalid numeric value
        /// </summary>
        /// <param name="result">
        /// result of validation
        /// </param>
        /// <param name="message">
        /// string storing error message
        /// </param>
        /// <param name="name">
        /// name of the parameter
        /// </param>
        /// <param name="tb">
        /// textbox where validation occurs
        /// </param>
        private void CheckNumeric(bool result, ref string message, string name,
            TextBox tb)
        {
            if (!result)
            {
                message += name + " musi być wartością numeryczną";
                SetTBState(tb,false);
            }
        }

        /// <summary>
        /// validation function, checks wheter textbox parameter is double type
        /// </summary>
        /// <param name="tb">
        /// textbox to validate
        /// </param>
        /// <param name="name">
        /// name of the parameter
        /// </param>
        /// <param name="value">
        /// output value
        /// </param>
        /// <param name="message">
        /// string to store error message
        /// </param>
        private void CheckIfDouble(TextBox tb, string name, out double value,
            ref string message)
        {
            bool result = double.TryParse(tb.Text, out value);
            CheckNumeric(result,ref message, name, tb);
            if (!result)
                message += " (z przecinkiem jako separatorem np. 0,95)" + Environment.NewLine +
                    "_________________________" + Environment.NewLine;
        }

        /// <summary>
        /// Same as CheckIfDouble but for long type
        /// </summary>
        /// <param name="name">
        /// name of the parameter
        /// </param>
        /// <param name="value">
        /// output value
        /// </param>
        /// <param name="message">
        /// string to store error message
        /// </param>
        private void CheckIfLong(TextBox tb, string name, out long value,
            ref string message)
        {
            bool result = long.TryParse(tb.Text, out value);
            CheckNumeric(result, ref message, name, tb);
            if (!result)
                message += " (liczba całkowita np. 1000)" + Environment.NewLine +
                    "_________________________" + Environment.NewLine;
        }

        /// <summary>
        /// Same as CheckIfDouble but for integer type
        /// </summary>
        /// <param name="name">
        /// name of the parameter
        /// </param>
        /// <param name="value">
        /// output value
        /// </param>
        /// <param name="message">
        /// string to store error message
        /// </param>
        private void CheckIfInt(TextBox tb, string name, out int value,
            ref string message)
        {
            bool result = int.TryParse(tb.Text, out value);
            CheckNumeric(result, ref message, name, tb);
            if (!result)
                message += " (liczba całkowita np. 15)" + Environment.NewLine +
                    "_________________________" + Environment.NewLine;
        }

        /// <summary>
        /// function set model tested icon ststus on intrface
        /// </summary>
        /// <param name="isTested">
        /// is model tested?
        /// </param>
        private void SetModelTested(bool isTested)
        {
            if (everTestRuned)
            {
                if (isTested)
                {
                    modelTested = true;
                    ModelStatSignal.Fill = new SolidColorBrush(Color.FromArgb(255, 0, 128, 0));
                }
                else
                {
                    modelTested = false;
                    ModelStatSignal.Fill = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
                }
            }
        }

        /// <summary>
        /// function shows error messag on the screen
        /// </summary>
        /// <param name="message">
        /// string holding error message
        /// </param>
        /// <returns>
        /// true if any errors occurs
        /// </returns>
        private bool CheckMessageError(string message)
        {
            if (message != string.Empty)
            {
                MessageBox.Show(message, "Wystąpiły błędy");
                return false;
            }
            return true;
        }

        /// <summary>
        /// clear model errors on the interface
        /// </summary>
        private void ClearTestModelErrors()
        {
            AddToLog("Czyszczenie informacji o błędach testu modelu");
            SetTBState(TextBox_Population, true);
            SetTBState(TextBox_kappa, true);
            SetTBState(TextBox_beta, true);
            SetTBState(TextBox_i, true);
            SetTBState(TextBox_I0, true);
        }


        //button start test click
        private void Button_StartTest_Click(object sender, RoutedEventArgs e)
        {
            string message = string.Empty;
            
            ClearTestModelErrors();

            AddToLog("Sprawdzanie formatu danych testowych modelu");
            CheckIfDouble(TextBox_beta, "Współczynnik zachorowalności", out beta, ref message);
            CheckIfDouble(TextBox_kappa, "Współczynnik zdrowienia", out kappa, ref message);
            CheckIfLong(TextBox_I0, "Początkowa wartość zainfekowanych", out IBeg, ref message);
            CheckIfLong(TextBox_Population, "Populacja", out epidemicsPopulation, ref message);
            CheckIfInt(TextBox_i, "Liczba okresów", out epidemicsLength, ref message);

            if (!CheckMessageError(message))
                return;

            AddToLog("Sprawdzanie poprawności danych testowych modelu");
            if (IBeg <= 0)
            {
                message += "Liczba zainfekowanych musi być większa od 0" + Environment.NewLine;
                SetTBState(TextBox_I0, false);
            }

            if (epidemicsPopulation <= 0)
            {
                message += "Populacja musi być większa od 0" + Environment.NewLine;
                SetTBState(TextBox_Population, false);
            }

            if (IBeg > epidemicsPopulation)
            {
                message += "Liczba zainfekowanych musi być mniejsza od populacji" + Environment.NewLine;
                SetTBState(TextBox_I0, false);
                SetTBState(TextBox_Population, false);
            }

            if (kappa >= 1.0 || kappa <= 0.0)
            {
                message += "Współczynnik zdrowienia musi być z przedziału (0,1)" + Environment.NewLine;
                SetTBState(TextBox_kappa, false);
            }

            if (beta >= 1.0 || beta <= 0)
            {
                message += "Współczynnik zachorowalności musi być z przedziału (0,1)" + Environment.NewLine;
                SetTBState(TextBox_beta, false);
            }

            if (!CheckMessageError(message))
                return;

            AddToLog("Inicjalizacja modelu");
            em.Initialize(epidemicsPopulation, IBeg, beta, kappa, 0, 0, 0);

            AddToLog("Analiza modelu");
            if (!em.Analyse(out message))
                if (!CheckMessageError(message))
                    return;

            TextBox_S0.Text = (epidemicsPopulation - IBeg).ToString();

            AddToLog("Czyszczenie starnych danych");
            SItems.Clear();
            IItems.Clear();
            RItems.Clear();

            AddToLog("Start symulacji");
            for (int i = 0; i < epidemicsLength; i++)
            {
                em.NextStep(0, 0);
                SItems.Add(new KeyValuePair<int, double>(i + 1, em.S));
                IItems.Add(new KeyValuePair<int, double>(i + 1, em.I));
                RItems.Add(new KeyValuePair<int, double>(i + 1, em.R));
            }

            AddToLog("Koniec symulacji");
            em.Finish();

            everTestRuned = true;
            SetModelTested(true);
        }

        /// <summary>
        /// check whether text box and its tested value are equal
        /// </summary>
        /// <param name="tb">
        /// textbox with desired value
        /// </param>
        /// <param name="value">
        /// string with current value
        /// </param>
        private void CheckModelTested(TextBox tb, string value)
        {
            if (tb.Text != value)
            {
                SetModelTested(false);
                if(everTestRuned)
                    TextBox_S0.Text = "-";
            }
            else
            {
                if (TextBox_Population.Text == epidemicsPopulation.ToString() &&
                     TextBox_I0.Text == IBeg.ToString() &&
                     TextBox_i.Text == epidemicsLength.ToString() &&
                     TextBox_beta.Text == beta.ToString() &&
                     TextBox_kappa.Text == kappa.ToString() &&
                     everTestRuned)
                {
                    SetModelTested(true);
                    TextBox_S0.Text = (epidemicsPopulation - IBeg).ToString();
                }
            }
        }


        //text box events - validation
        private void TextBox_Population_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckModelTested(TextBox_Population, epidemicsPopulation.ToString());
        }

        private void TextBox_I0_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckModelTested(TextBox_I0, IBeg.ToString()); ;
        }

        private void TextBox_i_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckModelTested(TextBox_i, epidemicsLength.ToString());
        }

        private void TextBox_beta_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckModelTested(TextBox_beta, beta.ToString());
        }

        private void TextBox_kappa_TextChanged(object sender, TextChangedEventArgs e)
        {
            CheckModelTested(TextBox_kappa, kappa.ToString());
        }

        /// <summary>
        /// clear algorithm errors
        /// </summary>
        private void ClearStartAlghoritmErrors()
        {
            AddToLog("Czyszczenie danych o błędach algorytmu");
            SetTBState(TextBox_AlgoPopulation, true);
            SetTBState(TextBox_MutationProb, true);
            SetTBState(TextBox_SimpleCrossProb, true);
            SetTBState(TextBox_Seed, true);
            SetTBState(TextBox_VaccineCost, true);
            SetTBState(TextBox_HospitalCost, true);
            SetTBState(TextBox_DeadCost, true);
            SetTBState(TextBox_MaxIt, true);
            SetTBState(TextBox_MaxDiff, true);
            SetTBState(TextBox_MaxVaccine, true);
            SetTBState(TextBox_MaxHospital, true);
            SetTBState(TextBox_VaccineEffect, true);
            SetTBState(TextBox_HospitalEffect, true);
            SetTBState(TextBox_lambda, true);
        }

        //button start event
        private void Button_Start_Click(object sender, RoutedEventArgs e)
        {
            string message = string.Empty;

            ClearStartAlghoritmErrors();

            if (!modelTested)
                message += "Model musi najpierw być przetestowany" + Environment.NewLine;

            if (!CheckMessageError(message))
                return;

            AddToLog("Sprawdzanie poprawności formatu danych algorytmu");
            // główne okienko

            if (Combobox_selectionMethod.SelectedIndex == 0)
                selectionMethod = Alghoritm.Alghoritm.Selection.Rullete;
            else
                selectionMethod = Alghoritm.Alghoritm.Selection.Ranking;
            CheckIfLong(TextBox_AlgoPopulation, "Populacja algorytmu", out populationCount, ref message);
            CheckIfDouble(TextBox_MutationProb, "Prawdopodobieństwo mutacji", out MutationProb, ref message);
            CheckIfDouble(TextBox_SimpleCrossProb, "Prawdopodobiństwo krzyżowania jednopunktowego", out simpleCrossProb, ref message);
            CheckIfInt(TextBox_Seed, "Ziarno prawdopodobieństwa", out seed, ref message);

            //dolne okienko
            CheckIfInt(TextBox_VaccineCost, "Koszt szczepionki", out vacCost, ref message);
            CheckIfInt(TextBox_HospitalCost, "Koszt pobytu w szpitalu", out hospCost, ref message);
            CheckIfInt(TextBox_DeadCost, "Koszt pogrzebu", out dedCost, ref message);
            CheckIfLong(TextBox_MaxIt, "Maksymalna ilość iteracji", out maxIt, ref message);
            CheckIfLong(TextBox_MaxDiff, "Maksymalna różnica przystosowania", out maxDiff, ref message);

            //prawe okienko
            CheckIfInt(TextBox_MaxVaccine, "Maksymalna ilość szczepionek", out maxVac, ref message);
            CheckIfInt(TextBox_MaxHospital, "Maksymalna ilość miejsc w szpitalach", out maxHosp, ref message);
            CheckIfDouble(TextBox_VaccineEffect, "Skuteczność szczepionek", out vacEffct, ref message);
            CheckIfDouble(TextBox_HospitalEffect, "Skuteczność hospitalizacji", out hospEffect, ref message);
            CheckIfDouble(TextBox_lambda, "Współczynnik śmiertelności", out lambda, ref message);

            if (!CheckMessageError(message))
                return;

            AddToLog("Analiza poprawności danych algorytmu");
            if (populationCount <= 1)
            {
                message += "Populacja algorytmu musi być większa od 1" + Environment.NewLine;
                SetTBState(TextBox_AlgoPopulation, false);
            }

            if (MutationProb < 0 || MutationProb > 1)
            {
                message += "Prawdopopdobieństwo mutacji musi być z przedziału <0,1>" + Environment.NewLine;
                SetTBState(TextBox_MutationProb, false);
            }

            if (simpleCrossProb < 0 || simpleCrossProb > 1)
            {
                message += "Prawdopodobieństwo krzyżowanie jednopunktowego musi być z przedziału <0,1>" + Environment.NewLine;
                SetTBState(TextBox_SimpleCrossProb, false);
            }

            if (MutationProb + simpleCrossProb > 1)
            {
                message += "Suma prawdopodobieństw nie może być większa od 1" + Environment.NewLine;
                SetTBState(TextBox_SimpleCrossProb, false);
                SetTBState(TextBox_MutationProb, false);
            }

            if (vacEffct <= 0 || vacEffct > 1)
            {
                message += "Skuteczność szczepionek musi być z przedziału (0,1>" + Environment.NewLine;
                SetTBState(TextBox_VaccineEffect, false);
            }

            if (hospEffect <= 0 || hospEffect > 1)
            {
                message += "Skuteczność hospitalizacji musi być z przedziału (0,1>" + Environment.NewLine;
                SetTBState(TextBox_HospitalEffect, false);
            }

            if (lambda <= 0 || lambda > kappa)
            {
                message += "Współczynnik śmiertelności musi być z przedziału (0,kappa)";
                SetTBState(TextBox_lambda, false);
            }

            if (maxDiff < 0)
            {
                message += "Maksymalna różnica nie może być mniejsza od 0" + Environment.NewLine;
                SetTBState(TextBox_MaxDiff, false);
            }

            if (maxIt < 0)
            {
                message += "Maksymalna liczba iteracji nie może być mniejsza od 0" + Environment.NewLine;
                SetTBState(TextBox_MaxIt, false);
            }

            if (maxIt == 0 && maxDiff == 0)
            {
                message += "Przynajmniej jedno kryterium stopu musi być aktywne" + Environment.NewLine;
                SetTBState(TextBox_MaxIt, false);
                SetTBState(TextBox_MaxDiff, false);
            }

            if (!CheckMessageError(message))
                return;

            AddToLog("Inicjalizacja zmodyfikowanego modelu");
            em.Initialize(epidemicsPopulation, IBeg, beta, kappa, lambda, vacEffct, hospEffect);

            AddToLog("Inicjalizacja algorytmu");
            al.Initialize(em, selectionMethod, epidemicsLength, populationCount, vacCost, hospCost,
                dedCost, maxVac, maxHosp, maxIt, maxDiff, MutationProb, simpleCrossProb, seed);

            AddToLog("Start algorytmu");
            al.Start();

            AddToLog("Koniec działania algrotymu");
            al.Finish();

            AddToLog("Czyszcenie starych danych algorytmu");
            ResultItems.Clear();
            WorstItems.Clear();
            AverageItems.Clear();

            if (CheckBox_isDraw.IsChecked == true)
            {
                AddToLog("Dodanie nowych danych");
                FIllResultCollection(ResultItems, al.GoalHistory);
                FIllResultCollection(WorstItems, al.WorstlHistory);
                FIllResultCollection(AverageItems, al.AvglHistory);
            }

            if (CheckBox_isSave.IsChecked == true)
            {
                AddToLog("Zapisywanie danych do pliku");
                saveResultsToFile();
            }

            AddToLog("Czyszczenie starego wyniku");
            TextBox_Results.Text = "";
            TextBox_Result.Text = "";

            AddToLog("Dodawanie informacji o wyniku");
            TextBox_Result.Text = al.GetResult().GoalValue.ToString();
            TextBox_Results.Text += (string.Format("{0,10} {1,12} {2,10}" + Environment.NewLine,
                "lp.","Szczepionki", "Szpitale"));

            for (int i = 0; i < al.GetResult().Members.Count(); i++)
                TextBox_Results.Text += (string.Format("{0,10} {1,12} {2,10}" + Environment.NewLine,
                i + 1, al.GetResult().Members[i].UseVaccines,
                al.GetResult().Members[i].UseHospitals));

            TextBox_Results.Text += string.Format("Mutacje: {0}, Krzyżowania jedptk: {1},{2}Krzyżowania wielptk: {3}{4}",
                al.MutationCount, al.SimpleCount, Environment.NewLine, al.MultiCrossingCount, Environment.NewLine);

            //TODO wywalic jakby co!!
            SItems.Clear();
            IItems.Clear();
            RItems.Clear();
            foreach (KeyValuePair<int, double> yo in al.S_history)
                SItems.Add(new KeyValuePair<int, double>(yo.Key,yo.Value));
            foreach (KeyValuePair<int, double> yo in al.I_history)
                IItems.Add(new KeyValuePair<int, double>(yo.Key, yo.Value));
            foreach (KeyValuePair<int, double> yo in al.R_history)
                RItems.Add(new KeyValuePair<int, double>(yo.Key, yo.Value));
            EpidemicsChart.Title = "Przebieg Epidemii po optymalizacji";

        }

        //filling plot collection
        private void FIllResultCollection(ObservableCollection<KeyValuePair<long, long>> values, List<long> history)
        {
            for (long i = 0; i < history.Count; i++)
            {
                if (i != 0 && i != (history.Count - 1) && (history[(int)i] == history[(int)(i - 1)])
                    && (history[(int)i] == history[(int)(i + 1)]))
                    continue;
                values.Add(new KeyValuePair<long, long>(i, history[(int)i]));
            }
        }

        //saving results to file
        private void saveResultsToFile()
        {
            if( TextBox_SaveFileName.Text == "" )
                Button_ChooseSaveFile_Click(this,null);

            string fileName = TextBox_SaveFileName.Text;

            string data = string.Format("{0,10}\t{1,10}\t{2,10}\t{3,10}" + Environment.NewLine,
                    "it", "Best", "Avg", "Worst");

            for (int i = 0; i < al.AvglHistory.Count; i++)
                data += string.Format("{0,10}\t{1,10}\t{2,10}\t{3,10}" + Environment.NewLine,
                    i, al.GoalHistory[i], al.AvglHistory[i], al.WorstlHistory[i]);

            File.WriteAllText(fileName, data);
        }

        //log panel functions
        private void Button_ClearLog_Click(object sender, RoutedEventArgs e)
        {
            ListBox_Log.Items.Clear();
        }

        private void AddToLog(string message)
        {
            ListBox_Log.Items.Add(message);
        }

        //button choose file event
        private void Button_ChooseSaveFile_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".text"; // Default file extension
            dlg.Filter = "Text documents (.txt)|*.txt"; // Filter files by extension 

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results 
            if (result == true)
            {
                // Save document 
                TextBox_SaveFileName.Text = dlg.FileName;
            }
        }

        //check box events
        private void CheckBox_isSave_Checked(object sender, RoutedEventArgs e)
        {
            Button_ChooseSaveFile.IsEnabled = true;
            TextBox_SaveFileName.IsEnabled = true;
        }

        private void CheckBox_isSave_Unchecked(object sender, RoutedEventArgs e)
        {
            Button_ChooseSaveFile.IsEnabled = false;
            TextBox_SaveFileName.IsEnabled = false;
        }
    }
}
