﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alghoritm
{
    //epidemics model class
    public class EpidemicsModel
    {
        //group values
        private double sNumber;
        private double iNumber;
        private double rNumber;
        private double dNumber;

        //begining group value
        private double iBegNumber;

        //standard model parameters
        private long population;
        private double beta;
        private double kappa;

        //modified model parameters
        private double lambda;
        private double vaccineEffect;
        private double hospitalizedEffect;

        //dynamic parameters
        private long iteration;

        //control values
        bool isInitialized;

        //Public properties
        public double S { get { return sNumber; } }
        public double I { get { return iNumber; } }
        public double R { get { return rNumber; } }
        public double D { get { return dNumber; } }
        public long Population { get { return population; } }


        /// <summary>
        /// Public constructor
        /// </summary>
        public EpidemicsModel()
        {
            isInitialized = false;
        }

        /// <summary>
        /// One step on model
        /// </summary>
        /// <param name="vaccine">
        /// number of vaccines in current step
        /// </param>
        /// <param name="hospitalized">
        /// number of hospitalized people in current step
        /// </param>
        public void NextStep(int vaccine, int hospitalized)
        {
            if (!isInitialized)
                throw new Exception("Object not initialized");

           
            double newDeaths = lambda * iNumber;
            double newRecovered = (kappa - lambda) * iNumber;
            double vacEffective = (vaccineEffect * vaccine) / population;
            if (vacEffective > sNumber)
                vacEffective = sNumber;
            double hospEffective = (hospitalizedEffect * hospitalized) / population;
            if (hospEffective > sNumber)
                hospEffective = sNumber;
            double infected = beta * (sNumber - hospEffective) * iNumber;

            

            sNumber = sNumber - infected - vacEffective;
            iNumber = iNumber + infected - newRecovered - newDeaths;
            rNumber = rNumber + newRecovered + vacEffective;
            dNumber = dNumber + newDeaths;

            iteration++;
        }

        /// <summary>
        /// Initialization of model
        /// </summary>
        /// <param name="begPopulation">
        /// population in epidemics model
        /// </param>
        /// <param name="BegINumber">
        /// initial infected number
        /// </param>
        /// <param name="betaParam">
        /// beta parameter
        /// </param>
        /// <param name="kappaParam">
        /// kappa parameter
        /// </param>
        /// <param name="lambdaParam">
        /// lambda parameter
        /// </param>
        /// <param name="vaccineEfectParam">
        /// vaccine effectiveness
        /// </param>
        /// <param name="hospitalizedEffectParam">
        /// hospitals effectiveness
        /// </param>
        public void Initialize(long begPopulation, long BegINumber, double betaParam,
            double kappaParam, double lambdaParam, double vaccineEfectParam,
            double hospitalizedEffectParam)
        {
            iteration = 0;

            population = begPopulation;
            iNumber = (double)(BegINumber) / population;
            iBegNumber = iNumber;
            sNumber = 1.0 - iNumber;
            rNumber = 0.0;
            dNumber = 0.0;

            beta = betaParam;
            kappa = kappaParam;

            lambda = lambdaParam;
            vaccineEffect = vaccineEfectParam;
            hospitalizedEffect = hospitalizedEffectParam;

            isInitialized = true;
        }

        /// <summary>
        /// Analysing function, check epidemy state
        /// </summary>
        /// <param name="message">
        /// stores error message
        /// </param>
        /// <returns>
        /// returns true if model is OK
        /// </returns>
        public bool Analyse(out string message)
        {
            if (iNumber == 0)
            {
                message = "Brak osobników zainfekowanych";
                return false;
            }

            if (beta * sNumber < kappa)
            {
                message = "Nie spełnione są założenia o wybuchu epidemii";
                return false;
            }

            message = "Model działa poprawnie";
            return true;
        }

        /// <summary>
        /// reseting model, no need to initialize again
        /// </summary>
        public void Reset()
        {
            iNumber = iBegNumber;
            sNumber = 1.0 - iNumber;
            rNumber = 0.0;
            dNumber = 0.0;

            isInitialized = true;
        }

        /// <summary>
        /// Finishing of model ( control function )
        /// </summary>
        public void Finish()
        {
            isInitialized = false;
        }
    }
}
