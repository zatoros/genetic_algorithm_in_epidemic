﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;

namespace Alghoritm
{
    /// <summary>
    /// algorithm class
    /// </summary>
    public class Alghoritm
    {
        /// <summary>
        /// class for storing ONE-iteration parameters
        /// </summary>
        public class SingleMember
        {
            private int useVaccines;
            private int useHospitals;

            public int UseVaccines { get { return useVaccines; } }
            public int UseHospitals { get { return useHospitals; } }

            public SingleMember(int vaccines = -1, int hospitals = -1)
	        {
                useVaccines = vaccines;
                useHospitals = hospitals;
        	}

            public SingleMember(SingleMember copy)
            {
                useVaccines = copy.useVaccines;
                useHospitals = copy.useHospitals;
            }
        }

        /// <summary>
        /// class for storing one RESULT
        /// </summary>
        public class Member : IComparable<Member>
        {
            public SingleMember[] Members { set; get; }

            public long GoalValue { set; get; }

            public Member(int size = 0)
            {
                if( size != 0 )
                    Members = new SingleMember[size];
            }

            public Member(Member copy)
            {
                Members = new SingleMember[copy.Members.Count()];
                for (int i = 0; i < Members.Count(); i++)
                    Members[i] = new SingleMember(copy.Members[i]);
                GoalValue = copy.GoalValue;
            }

            int IComparable<Member>.CompareTo(Member other)
            {
                if (GoalValue < other.GoalValue)
                    return -1;
                else if (GoalValue == other.GoalValue)
                    return 0;
                else
                    return 1;
            }
        }

        public enum Selection { Rullete, Ranking };
        public enum Operator { Mutation, SimpleCrossing, MultiPointCrossing };

        //probability of opearators
        double mutationProb;
        double simpleProb;

        //global random
        Random rand;

        //population variables
        private Member[] population;
        private long populationCount;
        private int epidemicsLength;

        //selection method
        Selection selectionMethod;

        //goal function parameters
        private int vaccineCost;
        private int hospitalCost;
        private int deadCost;

        //maximum parameters annout
        private int maxVaccineCount;
        private int maxHospitalCount;

        //stop
        private long maxIteration;
        private double maxDifference;

        //epidemics model variable
        private EpidemicsModel epidemicsModel;

        //control value
        bool isInitialized;
        long iteration;

        //log values
        List<long> goalHistory;
        public List<long> GoalHistory { get { return goalHistory; } }

        List<long> worstHistory;
        public List<long> WorstlHistory { get { return worstHistory; } }

        List<long> avgHistory;
        public List<long> AvglHistory { get { return avgHistory; } }

        //more log values
        public ObservableCollection<KeyValuePair<int, double>> s_history;
        public ObservableCollection<KeyValuePair<int, double>> S_history { get { return s_history; } }

        public ObservableCollection<KeyValuePair<int, double>> i_history;
        public ObservableCollection<KeyValuePair<int, double>> I_history { get { return i_history; } }

        public ObservableCollection<KeyValuePair<int, double>> r_history;
        public ObservableCollection<KeyValuePair<int, double>> R_history { get { return r_history; } }

        //statistic values
        private long mutCount;
        public long MutationCount { get { return mutCount; } }
        private long simCount;
        public long SimpleCount { get { return simCount; } }
        private long crsCount;
        public long MultiCrossingCount { get { return crsCount; } }

        /// <summary>
        /// generating begining population
        /// </summary>
        /// <param name="seed">
        /// random seed - same seed -> same begining population
        /// </param>
        private void GeneratePopulation(int seed = -1)
        {
            for (int i = 0; i < populationCount; i++)
            {
                for (int j = 0; j < epidemicsLength; j++)
                {
                    population[i].Members[j] = GenerateMember();
                }
                population[i].GoalValue = CountCost(population[i].Members);
            }

            Array.Sort<Member>(population);
        }

        /// <summary>
        /// function generates random single member
        /// </summary>
        /// <returns>
        /// new member
        /// </returns>
        private SingleMember GenerateMember()
        {
            return new SingleMember(rand.Next(0, maxVaccineCount), 
                rand.Next(0, maxHospitalCount));
        }

        /// <summary>
        /// function simulates epidemy for one population member
        /// and count goal function for it
        /// </summary>
        /// <param name="members">
        /// solution
        /// </param>
        /// <returns>
        /// cost
        /// </returns>
        private long CountCost(SingleMember[] members)
        {
            long cost = 0;

            epidemicsModel.Reset();
            foreach(SingleMember sm in members)
            {
                epidemicsModel.NextStep(sm.UseVaccines, sm.UseHospitals);
                cost += sm.UseVaccines * vaccineCost;
                cost += sm.UseHospitals * hospitalCost;
            }
            epidemicsModel.Finish();

            cost += (long)((epidemicsModel.D * epidemicsModel.Population) * deadCost);
            return cost;
        }

        /// <summary>
        /// rulette selection method
        /// </summary>
        /// <param name="count">
        /// numbers of member to select
        /// </param>
        /// <returns>
        /// selected membrs
        /// </returns>
        private Member[] ChooseMembersRulette(int count) 
        {
            Member[] selectedMembers = new Member[count];

            long sumValues = 0;
            int index = 0;

            byte[] randByte = new byte[8];
            long converted;

            foreach (Member mem in population)
                sumValues += mem.GoalValue;

            long curProb;
            for (int i = 0; i < populationCount; i++)
            {
                curProb = 0;

                rand.NextBytes(randByte);
                converted = BitConverter.ToInt64(randByte, 0);
                if (converted < 0)
                    converted = -converted;
                converted %= (sumValues + 1);

                for (int j = 1; j < (count - index); j++)
                    curProb += population[populationCount - j].GoalValue;
                curProb += population[i].GoalValue;

                if (converted <= curProb)
                {
                    selectedMembers[index] = new Member(population[i]);
                    if (++index == count)
                        break;
                }

                sumValues -= population[i].GoalValue;
            }

            return selectedMembers;
        }

        /// <summary>
        /// ranking selection method
        /// </summary>
        /// <param name="count">
        /// numbers of member to select
        /// </param>
        /// <returns>
        /// selected membrs
        /// </returns>
        private Member[] ChooseMemberRanking(int count)
        {
            Member[] selectedMembers = new Member[count];

            long sumValues = (populationCount * (populationCount - 1)) / 2;
            int index = 0;

            byte[] randByte = new byte[8];
            long converted;

            long curProb;
            for (int i = 0; i < populationCount; i++)
            {
                curProb = 0;

                rand.NextBytes(randByte);
                converted = BitConverter.ToInt64(randByte, 0);
                if (converted < 0)
                    converted = -converted;
                converted %= (sumValues + 1);

                for (int j = 1; j < (count - index); j++)
                    curProb += j - 1;
                curProb += populationCount - i - 1;

                if (converted <= curProb)
                {
                    selectedMembers[index] = new Member(population[i]);
                    if (++index == count)
                        break;
                }

                sumValues -= (populationCount - i - 1);
            }
            return selectedMembers;
        }

        /// <summary>
        /// random genetic operator
        /// </summary>
        /// <returns>
        /// oprator
        /// </returns>
        private Operator ChooseOperator()
        {
            double generated = rand.NextDouble();
            if (generated < mutationProb)
                return Operator.Mutation;
            else if (generated < simpleProb + mutationProb)
                return Operator.SimpleCrossing;
            else
                return Operator.MultiPointCrossing;
        }

        /// <summary>
        /// generate new member
        /// </summary>
        /// <returns>
        /// member
        /// </returns>
        private Member GenerateNewMembers() 
        {
            Operator op = ChooseOperator();

            if (op == Operator.Mutation)
            {
                mutCount++;
                Member mutated;

                if (selectionMethod == Selection.Rullete)
                    mutated = ChooseMembersRulette(1).First();
                else
                    mutated = ChooseMemberRanking(1).First();

                mutated.Members[rand.Next(0, epidemicsLength)] = GenerateMember();

                return new Member(mutated);
            }
            else
            {
                Member[] crossing;

                if (selectionMethod == Selection.Rullete)
                    crossing = ChooseMembersRulette(2);
                else
                    crossing = ChooseMemberRanking(2);

                if (op == Operator.MultiPointCrossing)
                {
                    crsCount++;
                    for (int i = 0; i < epidemicsLength; i++)
                        if (rand.NextDouble() < 0.5)
                            crossing[0].Members[i] = crossing[1].Members[i];
                }
                else
                {
                    simCount++;
                    int separator = rand.Next(1, epidemicsLength);
                    for (int i = 0; i < separator; i++)
                        crossing[0].Members[i] = crossing[1].Members[i];
                }
                return new Member(crossing[0]);
            }
        }

        /// <summary>
        /// function try to put new member to population
        /// </summary>
        /// <param name="member">
        /// member to put
        /// </param>
        private void PutNewMembers(Member member)
        {
            member.GoalValue = CountCost(member.Members);
            if (member.GoalValue < population.Last().GoalValue)
                population[populationCount - 1] = member;
            Array.Sort<Member>(population);

        }
        
        /// <summary>
        /// check stop criteria
        /// </summary>
        /// <returns>
        /// true - stop, false - don't stop
        /// </returns>
        private bool IsStop()
        {
            return
                ((maxIteration != 0 && iteration > maxIteration) ||
                (maxDifference != 0 && 
                GetDifference() < maxDifference));
        }

        /// <summary>
        /// one iteration of alghoritm
        /// </summary>
        private void NextIteration()
        {
            PutNewMembers(GenerateNewMembers());
            iteration++;
        }
        
        /// <summary>
        /// return current result
        /// </summary>
        /// <returns>
        /// member - result
        /// </returns>
        public Member GetResult()
        {
            return population.First();
        }

        /// <summary>
        /// return worst current result
        /// </summary>
        /// <returns>
        /// member - worst result
        /// </returns>
        public Member GetWorst()
        {
            return population.Last();
        }

        /// <summary>
        /// average cost of current population
        /// </summary>
        /// <returns>
        /// average cost
        /// </returns>
        public long GetAvarage()
        {
            long sum = 0;
            foreach (Member m in population)
                sum += m.GoalValue;

            return sum / population.Count(); 
        }

        /// <summary>
        /// get difference between worst and best cost in current population
        /// </summary>
        /// <returns>
        /// cost difference
        /// </returns>
        public long GetDifference()
        {
            return population.Last().GoalValue -population.First().GoalValue;
        }

        /// <summary>
        /// function save statistic cost values
        /// </summary>
        private void saveLog()
        {
            goalHistory.Add(GetResult().GoalValue);
            worstHistory.Add(GetWorst().GoalValue);
            avgHistory.Add(GetAvarage());
        }

        /// <summary>
        /// alghoritm start
        /// </summary>
        public void Start()
        {
            GeneratePopulation();
            saveLog();

            rand = new Random();

            while (!IsStop())
            {
                NextIteration();
                saveLog();
            }
            Finish();
        }

        /// <summary>
        /// alghoritm initialization
        /// </summary>
        /// <param name="model">
        /// epidemics model
        /// </param>
        /// <param name="sel">
        /// selection method
        /// </param>
        /// <param name="epiLength">
        /// epidemy length
        /// </param>
        /// <param name="popCount">
        /// population count (alghoritm!)
        /// </param>
        /// <param name="vacCost">
        /// cost of vaccine
        /// </param>
        /// <param name="hostCost">
        /// cost of hospitalization
        /// </param>
        /// <param name="dCost">
        /// deaths cost
        /// </param>
        /// <param name="maxVac">
        /// maximum vaccines
        /// </param>
        /// <param name="maxHost">
        /// maximum hospitalization
        /// </param>
        /// <param name="maxIt">
        /// maximal itteraton
        /// </param>
        /// <param name="maxDiff">
        /// maximum difference between best and worst cost
        /// </param>
        /// <param name="mutProb">
        /// mutation probability
        /// </param>
        /// <param name="simProb">
        /// one-point crosing probability
        /// </param>
        /// <param name="seed">
        /// random seed
        /// </param>
        public void Initialize(EpidemicsModel model, Selection sel,
            int epiLength,
            long popCount, int vacCost, int hostCost,
            int dCost, int maxVac, int maxHost, long maxIt,
            long maxDiff, double mutProb, double simProb,
            int seed = -1)
        {
            epidemicsModel = model;

            selectionMethod = sel;

            vaccineCost = vacCost;
            hospitalCost = hostCost;
            deadCost = dCost;

            populationCount = popCount;
            epidemicsLength = epiLength;
            population = new Member[popCount];
            for (int i = 0; i < popCount; i++)
                population[i] = new Member(epiLength);

            maxVaccineCount = maxVac;
            maxHospitalCount = maxHost;
            
            maxIteration = maxIt;
            maxDifference = maxDiff;

            mutationProb = mutProb;
            simpleProb = simProb;

            if( seed != -1 )
                rand = new Random(seed);
            else
                rand = new Random();
            
            iteration = 0;
            goalHistory = new List<long>();
            worstHistory = new List<long>();
            avgHistory = new List<long>();
            s_history = new ObservableCollection<KeyValuePair<int, double>>();
            i_history = new ObservableCollection<KeyValuePair<int, double>>();
            r_history = new ObservableCollection<KeyValuePair<int, double>>(); 


            isInitialized = true;
            mutCount = 0;
            simCount = 0;
            crsCount = 0;
        }

        /// <summary>
        /// alghoritm finish and generation statistics information
        /// </summary>
        public void Finish()
        {
            Member mem = GetResult();
           int i = 0;
            epidemicsModel.Reset();
            
            s_history.Clear();
            i_history.Clear();
            r_history.Clear();

            s_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.S));
            i_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.I));
            r_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.R + epidemicsModel.D));
            foreach (SingleMember sm in mem.Members)
            {
                i++;
                epidemicsModel.NextStep(sm.UseVaccines, sm.UseHospitals);
                s_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.S));
                i_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.I));
                r_history.Add(new KeyValuePair<int,double>(i,epidemicsModel.R + epidemicsModel.D));
            }
            epidemicsModel.Finish();

            isInitialized = false;
        }
        
    }
}
